from flask import Flask, request, jsonify
from db import db
import json
from bson import ObjectId

app = Flask(__name__)

@app.route("/", methods=["GET"])
def hello():
    return "Hello, World!"

@app.route("/user", methods=["POST"])
def createUser():
    data = request.json
    try:
        db["user"].insert(data)
        return jsonify({"message":"create user success", "status":200})
    except Exception as e:
        print(e)
        return jsonify({"message":"create user fail", "status":500})

@app.route("/userByClass/<className>",methods=["GET"])
def getUserByClass(className=None):
    if className is not None:
        res = list(db["user"].find({"class":str(className)}))
        return json.dumps(res,default=lambda item: str(item) if isinstance(item,ObjectId) else item)

@app.route("/login",methods=["POST"])
def login():
    data = request.json
    if "username" in data and "password" in data:
        try:
            res = list(db["user"].find({"username":data["username"],"password":data["password"]},{"username":0,"password":0}))
            if len(res)!=0:
                return json.dumps(res,default=lambda item: str(item) if isinstance(item,ObjectId) else item)
            else:
                return jsonify({})
        except Exception as e:
            print(e)
            return {}

@app.route("/updateUserById/<id>",methods=["PUT"])
def updateUserById(id=None):
    data =  request.json
    if id is None:
        return jsonify({"msg": "nothing to update"})
    try:
        db["user"].update_many({"_id":ObjectId(id)},{"$set":data})
        return jsonify({"msg": "update success", "status": 200})
    except Exception as e:
        print(e)
        return jsonify({"msg": "update fail","status":500})

