from app import app
import setting

if __name__ == '__main__':
    app.run(host=setting.FLASK_HOST,port=setting.FLASK_PORT,debug=setting.FLASK_DEBUG)