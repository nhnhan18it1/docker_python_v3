import pymongo

import setting

# mongoClient = pymongo.MongoClient(
#     f"mongodb://{settings.MONGO_USER}:{settings.MONGO_PASSWORD}"
#     f"@{settings.MONGO_HOST}:{settings.MONGO_PORT}/?authSource=admin"
# )
con_str = (f"mongodb://{setting.MONGOUSER}:{setting.MONGOPASS}"
    f"@{setting.MONGOHOST}:{setting.MONGOPORT}/?authSource=admin"
)
print("Connection string:", con_str)
mongoClient = pymongo.MongoClient(con_str)
db = mongoClient["dcpython"]
print(db.list_collection_names())